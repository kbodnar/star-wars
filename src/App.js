import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import './App.css';
import Menu from './components/menu';
import People from './pages/people';
import Films from './pages/films';
import Planets from './pages/planets';
import ItemCard from './pages/item-card';
import StoreProvider from './contexts/store-context';

function App() {

  return (
    <Router>
      <StoreProvider>
      <div className="App">
        <header className="app-header">
          <Menu />
        </header>

        <main className='main-container'>
          <Switch>
            <Route path='/people' component={People}/>
            <Route path='/films' component={Films}/>
            <Route path='/planets' component={Planets}/>
            <Route path='/item-card/:entityName/:entityId' component={ItemCard}/>
          </Switch>
        </main>
      </div>
      </StoreProvider>
    </Router>
  );
}

export default App;
