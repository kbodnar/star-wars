export const ENTITY = {
    PEOPLE: 'people',
    FILMS: 'films',
    PLANETS: 'planets'
}

export const SCHEMA = {
    films:  {
        title: null,
        director: null,
        producer: null,
        release_date: null,
        url: null
    },
    people: {
        name: null,
        mass: null,
        height: null,
        gender: null,
        skin_color: null,
        eye_color: null
    },
    planets: {
      name: null,
      climate: null,
      diameter: null,
      orbital_period: null
    }
}
