import { useState, useEffect } from 'react'
import { useStoreContext } from '../../contexts/store-context'
import mark from '../../assets/star_favorite_mark.png'
import unmark from '../../assets/star_favorite_unmark.png'

const Favorite = ({ isFavorite, params }) => {
    const { switchFavorite } = useStoreContext()
    const [isMark, setIsMark] = useState(isFavorite)

    useEffect(() => {
        setIsMark(isFavorite)
    }, [isFavorite])

    const changeFavorite = () => {
        setIsMark(prevState => !prevState)
        switchFavorite(params)
    }
    return (
        <div className='favorite'>
            <img src={isMark ? mark : unmark} onClick={changeFavorite}/>
        </div>
    )
};

export default Favorite