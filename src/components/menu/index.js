import { NavLink } from 'react-router-dom'
const Menu = () => {

    return (
        <div className='app-menu'>
            <NavLink to='/people' className='app-menu-item' activeClassName='app-menu-item-active'>People</NavLink>
            <NavLink to='/films' className='app-menu-item' activeClassName='app-menu-item-active'>Films</NavLink>
            <NavLink to='/planets' className='app-menu-item' activeClassName='app-menu-item-active'>Planets</NavLink>
        </div>
    )
}

export default Menu