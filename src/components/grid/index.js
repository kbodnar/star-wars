import Loader from '../loader'
import { useHistory } from 'react-router-dom'

const GridTable = (props) => {

    const { columns = [], rows = [], entity = ''} = props;
    
    const history = useHistory()

    const goToItem = id => {
        history.push(`item-card/${entity}/${id}`)
    }

    return (
        <div className='grid-table'>
            { !Boolean(rows?.length) && <Loader />}
            <table className="table table-striped">
                <thead>
                    <tr>
                        { columns && columns.map((column, idx) => <th key={idx}>{column.title}</th>)}
                    </tr>
                </thead>
                <tbody>
                    { rows && rows.map((row, idRow) => <tr key={idRow} className='grid-table-row' onClick={() => { goToItem(idRow)}}>
                        {
                            columns.map((el, idCol) => <td key={idCol} className='grid-table-cell'>{row[el.field]}</td>)
                        }
                     </tr>)
                    }
                </tbody>
            </table>
        </div>
    )
}

export default GridTable;