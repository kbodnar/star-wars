import { useContext, createContext, useReducer, useEffect, useState } from 'react'
import api from '../api'
import { ENTITY, SCHEMA } from '../constants'

const uniqueId = () => Date.now().toString(16) + Math.random().toString(16).substring(2);

export const StoreContext = createContext({
  films: null,
  people: null,
  planets: null,
  entity: null,
  schema: null,
  emitEntity: () => {},
  getEntity: () => {},
  clearEntity: () => {},
  switchFavorite: () => {}
})

export const useStoreContext = () => useContext(StoreContext)

const initialState = {
  films: null,
  people: null,
  planets: null 
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'SAVE_FILMS':
      return {
        ...state,
        films: action.payload
      }
      case 'SAVE_PEOPLE':
        return {
            ...state,
            people: action.payload
        }
      case 'SAVE_PLANETS':
        return {
            ...state,
            planets: action.payload
        }
      case 'SWITCH_FAVORITE':
        const { entityName, entityId } = action.payload
        const copyArr = [...state[entityName]]
        const item = state[entityName][entityId]
        const updatedItem = {
            ...item,
            isFavorite: !item.isFavorite
        }
        copyArr[entityId] = updatedItem
        return {
            ...state,
            [entityName]: copyArr
        }
    default:
      return state;
  }
}

const StoreProvider = ({ children }) => {
  const [entity, setEntity] = useState([])
  const [schema, setSchema] = useState({})
  const [url, setUrl] = useState(null)
  const [state, dispatch] = useReducer(reducer, initialState);

  const { FILMS, PEOPLE, PLANETS } = ENTITY

  const saveFilms = (payload) => dispatch({ type: 'SAVE_FILMS', payload})

  const savePeople = (payload) => dispatch({ type: 'SAVE_PEOPLE', payload})

  const savePlanets = (payload) => dispatch({ type: 'SAVE_PLANETS', payload})

  const switchFavorite = (payload) => dispatch({ type: 'SWITCH_FAVORITE', payload})


  const emitEntity = url => setUrl(url)


  const getEntity = ({ entityName, entityId}) => {
    try{
        setSchema(SCHEMA[entityName])
        setEntity(state[entityName][entityId])
      } catch {
        setEntity({})
      }
  }

  const clearEntity = () => {
      setEntity(null)
  }

  const saveToContext = (url, data) => {
    let decoratedData = data.map((item, id) => ({ id, isFavorite: false, ...item}))
    switch(url) {
        case PEOPLE:
           savePeople(decoratedData)
           break;
        case FILMS:
           saveFilms(decoratedData)
           break;
        case PLANETS:
           savePlanets(decoratedData)
           break;
    }
}

  useEffect(() => {
   
    const getData = async () => {
      if(!state[url]) {
        const response = await api.get(url)
        const { results } = await response.json()
        saveToContext(url, results)
      }
    }
      
   getData()
   
   }, [url])

  const storeValue ={
      emitEntity,
      getEntity,
      clearEntity,
      switchFavorite,
      entity,
      schema,
      ...state
  }
    return(
        <StoreContext.Provider value={storeValue}>
                { children }
        </StoreContext.Provider>
    )
}

export default StoreProvider