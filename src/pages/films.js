import { useEffect } from 'react'
import GridTable from '../components/grid'
import { useStoreContext } from '../contexts/store-context'
import { ENTITY } from '../constants'


const columns = [
    { title: 'Title', field: 'title'},
    { title: 'Director', field: 'director'},
    { title: 'Producer', field: 'producer'}
]

const Films = () => {
    const { films, emitEntity } = useStoreContext()

    useEffect(() => {
        if(!films) {
            emitEntity(ENTITY.FILMS)
        }
    }, [])

    return(
        <div className='page'>
             <GridTable
                entity={ENTITY.FILMS}
                columns={columns}
                rows={films}
             />
        </div>
    )
}

export default Films