import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useStoreContext } from '../contexts/store-context'

import Favorite from '../components/favorite'

const ItemCard = () => {

    const params = useParams()

    const { entity, getEntity, clearEntity, schema } = useStoreContext()

    useEffect(() => {
        getEntity(params)

        return () => clearEntity()
    }, [])

    return (
        <div className='item-card'>
            <Favorite isFavorite={entity?.isFavorite} params={params}/>
            <table className="table table-striped">
             <tbody>
                {
                 entity && Object.keys(schema).map((key, idx) => <tr key={idx}>
                     <td>{key}</td>
                     <td>
                         <div className='value-truncate'>
                             {entity[key]}
                         </div>
                     </td>
                 </tr>)
                }
             </tbody>
            </table>
        </div>
    )
}

export default ItemCard