import { useEffect } from 'react'
import { useStoreContext } from '../contexts/store-context'
import GridTable from '../components/grid'
import { ENTITY } from '../constants'

const columns = [
    { title: 'Name', field: 'name'},
    { title: 'Climate', field: 'climate'},
    { title: 'Diameter', field: 'diameter'},
    { title: 'Period', field: 'orbital_period'}
]

const Planets = () => {
    const { planets, emitEntity } = useStoreContext()
    const { PLANETS } = ENTITY

    useEffect(() => {
        if(!planets) {
            emitEntity(PLANETS)
        }
    }, [])

    return(
        <div className='page'>
            <GridTable
              entity={PLANETS}
              columns={columns}
              rows={planets}
             />
        </div>
    )
}

export default Planets