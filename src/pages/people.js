import { useEffect } from 'react'
import GridTable from '../components/grid'
import { useStoreContext } from '../contexts/store-context'
import { ENTITY } from '../constants'

const columns = [
    { title: 'Name', field: 'name'},
    { title: 'Mass', field: 'mass'},
    { title: 'Height', field: 'height'},
    { title: 'Eye', field: 'eye_color'}
]
const People = () => {
const { people, emitEntity } = useStoreContext()

useEffect(() => {
    if(!people) {
        emitEntity(ENTITY.PEOPLE)
    }
}, [])

 return(
        <div className='page'>
            <GridTable
              entity={ENTITY.PEOPLE}
              columns={columns}
              rows={people}
             />
        </div>
    )
}

export default People