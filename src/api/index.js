const base_url = 'https://swapi.dev/api'

export default {
    async get(query) {
        return fetch(`${base_url}/${query}`)
    },
}